/*
 * Copyright 2018 Rubén Beltrán del Río
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

/*
 * Declarations
 */

/* Constants */

const ngx_uint_t WEEK_LENGTH = 7;
const char * CLOSED_TOKEN = "closed";
const ngx_str_t TIME_REGEX = ngx_string("([0-9]{1,2})(?:\\:([0-9]{2}))?\\-([0-9]{1,2})(?:\\:([0-9]{2}))?");
const ngx_str_t HEAD_HTML = ngx_string("<!doctype html><html><head><title>This website is currently closed!</title></head><body><h1>This website is currently closed!</h1><p>This website has closed for the day, please check our office hours below</p><ul>");
const ngx_str_t MIDDLE_HTML = ngx_string("</ul><p><em>Current Server Time is: ");
const ngx_str_t FOOT_HTML = ngx_string("</em></p></body></html>");
const char * DAY_NAMES[7] = {
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
};

/* Main Configuration Structure */

typedef struct {
    ngx_array_t *office_hours;
} ngx_http_office_hours_conf_t;

/* Lifecycle Functions For Module Context */

static void *ngx_http_office_hours_create_conf(ngx_conf_t * cf);
static char *ngx_http_office_hours_merge_conf(ngx_conf_t * cf,
        void *parent, void *child);
static ngx_int_t ngx_http_office_hours_init(ngx_conf_t * cf);

/* Configuration Handler */

static char *ngx_http_office_hours(ngx_conf_t * cf, ngx_command_t * cmd,
                                   void *conf);

/* Filter Storage */

static ngx_http_output_header_filter_pt ngx_http_next_header_filter;
static ngx_http_output_body_filter_pt ngx_http_next_body_filter;

/* Utility Functions */
static ngx_uint_t ** parse_office_hours(ngx_array_t * office_hours);
static ngx_uint_t * parse_office_hours_string(ngx_str_t office_hours);
static ngx_flag_t within_office_hours(ngx_uint_t ** office_hours);
static ngx_uint_t get_day_of_week(time_t time);
static ngx_uint_t get_seconds_of_day(time_t time);
static ngx_uint_t parse_number(ngx_str_t string, ngx_uint_t start, ngx_uint_t end);
static ngx_str_t create_output_html(ngx_http_office_hours_conf_t * conf);
static char * format_hours(ngx_uint_t * seconds, char * day);
static char * format_seconds(ngx_uint_t seconds);
static char * left_pad(unsigned int number);

/* Globals */
ngx_regex_compile_t rc;
ngx_str_t OUTPUT_HTML;

/*
 * Module Definitions
 */

/* Module Directives */

static ngx_command_t ngx_http_office_hours_commands[] = {
    {
        ngx_string("office_hours"),
        NGX_HTTP_MAIN_CONF | NGX_HTTP_SRV_CONF | NGX_HTTP_LOC_CONF | NGX_CONF_TAKE1 | NGX_CONF_TAKE2 | NGX_CONF_TAKE3 | NGX_CONF_TAKE4 | NGX_CONF_TAKE5 | NGX_CONF_TAKE6 | NGX_CONF_TAKE7,
        ngx_http_office_hours,
        NGX_HTTP_LOC_CONF_OFFSET,
        offsetof(ngx_http_office_hours_conf_t, office_hours),
        NULL
    },

    ngx_null_command
};


/* Module Context */

static ngx_http_module_t ngx_http_office_hours_filter_module_ctx = {
    NULL,			/* Preconfiguration */
    ngx_http_office_hours_init,	/* Postconfiguration */

    NULL,			/* Create main configuration */
    NULL,			/* Initialize main configuration */

    NULL,			/* Create server configuration */
    NULL,			/* Merge server configuration */

    ngx_http_office_hours_create_conf,	/* Create location configuration */
    ngx_http_office_hours_merge_conf	/* Merge location configuration */
};


/* Module Definition */

ngx_module_t ngx_http_office_hours_filter_module = {
    NGX_MODULE_V1,		//Module Version
    &ngx_http_office_hours_filter_module_ctx,	//Module context
    ngx_http_office_hours_commands,	//Module commands
    NGX_HTTP_MODULE,		//Module Type
    NULL,			//Initialize Master
    NULL,			//Initialize Module
    NULL,			//Initialize Process
    NULL,			//Initialize Thread
    NULL,			//Exit Thread
    NULL,			//Exit Process
    NULL,			//Exit Master
    NGX_MODULE_V1_PADDING
};


/*
 * Main Header Filter
 * If the current time is within office hours, it goes to the next
 * handler. Otherwise it sets the headers to 403
 */

static ngx_int_t
ngx_http_office_hours_header_filter(ngx_http_request_t * r)
{

    ngx_uint_t ** parsed_office_hours;
    ngx_http_office_hours_conf_t *conf;

    conf =
        ngx_http_get_module_loc_conf(r,
                                     ngx_http_office_hours_filter_module);


    if (conf->office_hours == NULL) {
        ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                      "Office hours disabled");
        return ngx_http_next_header_filter(r);
    }

    parsed_office_hours = parse_office_hours(conf->office_hours);

    if (within_office_hours(parsed_office_hours)) {
        ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                      "Within office hours");
        return ngx_http_next_header_filter(r);
    }

    ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                  "Outside office hours");

    if (OUTPUT_HTML.data == NULL) {
        OUTPUT_HTML = create_output_html(conf);
    }

    r->headers_out.status = NGX_HTTP_FORBIDDEN;
    r->headers_out.content_length_n = OUTPUT_HTML.len;

    return ngx_http_next_header_filter(r);
}

/*
 * Main Body Filter
 * If the current time is within office hours, it goes to the next
 * handler. Otherwise it replaces the body with the office hours.
 */

static ngx_int_t
ngx_http_office_hours_body_filter(ngx_http_request_t * r, ngx_chain_t *in)
{

    ngx_buf_t    *b;
    ngx_uint_t ** parsed_office_hours;
    ngx_chain_t   out;
    ngx_http_office_hours_conf_t *conf;

    conf =
        ngx_http_get_module_loc_conf(r,
                                     ngx_http_office_hours_filter_module);


    if (conf->office_hours == NULL) {
        ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                      "Office hours disabled");
        return ngx_http_next_body_filter(r, in);
    }

    parsed_office_hours = parse_office_hours(conf->office_hours);

    if (within_office_hours(parsed_office_hours)) {
        ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                      "Within office hours");
        return ngx_http_next_body_filter(r, in);
    }

    ngx_log_error(NGX_LOG_DEBUG, r->connection->log, 0,
                  "Outside office hours");

    if (OUTPUT_HTML.data == NULL) {
        OUTPUT_HTML = create_output_html(conf);
    }

    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));
    if (b == NULL) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Failed to allocate response buffer.");
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    out.buf = b;
    out.next = NULL;

    b->start = OUTPUT_HTML.data;
    b->pos = b->start;
    b->end = OUTPUT_HTML.data + OUTPUT_HTML.len;
    b->last = b->end;

    b->memory = 1;
    b->last_buf = 1;

    return ngx_http_next_body_filter(r, &out);
}

/*
 * Callback for `office_hours ` directive
 * Reads the configuration loaded from the config file(cf)
 * And writes it to the right place in the module configuration(conf)
 */

static char *ngx_http_office_hours(ngx_conf_t * cf, ngx_command_t * cmd,
                                   void *conf)
{

    char *conf_structure = conf;

    ngx_str_t *hours, *value;
    ngx_array_t **office_hours;
    ngx_uint_t i;

    /* Gets the array from the config structure using the defined
     * offset, and if the pointer is unset it creates a new one.
     * (The first element is the directive itself, so we should be
     * offset by 1)
     */
    office_hours = (ngx_array_t **) (conf_structure + cmd->offset);

    if (*office_hours == NGX_CONF_UNSET_PTR) {
        *office_hours = ngx_array_create(cf->pool, cf->args->nelts - 1,
                                         sizeof(ngx_str_t));

        if (*office_hours == NULL) {
            return NGX_CONF_ERROR;
        }
    }
    value = cf->args->elts;

    for (i = 1; i < cf->args->nelts; ++i) {
        hours = ngx_array_push(*office_hours);
        if (hours == NULL) {
            return NGX_CONF_ERROR;
        }
        *hours = value[i];
    }

    return NGX_CONF_OK;
}


/*
 * Config Creator
 * Initializes the configuration structure
 */

static void *ngx_http_office_hours_create_conf(ngx_conf_t * cf)
{

    ngx_http_office_hours_conf_t *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_office_hours_conf_t));

    if (conf == NULL) {
        return NULL;
    }
    conf->office_hours = NGX_CONF_UNSET_PTR;

    return conf;
}

/*
 * Merge Config Values
 * Sets the defaults for the configuration and merges
 * with other configurations
 */

static char *ngx_http_office_hours_merge_conf(ngx_conf_t * cf,
        void *parent, void *child)
{

    ngx_http_office_hours_conf_t *prev = parent;
    ngx_http_office_hours_conf_t *conf = child;

    ngx_conf_merge_ptr_value(conf->office_hours, prev->office_hours, NULL);

    return NGX_CONF_OK;
}

/*
 * Parse the office hour strings in the configuration file
 * to fill out the hours array (in seconds)
 */

static ngx_uint_t ** parse_office_hours(ngx_array_t * office_hours)
{

    ngx_str_t *hours;
    ngx_uint_t ** parsed_office_hours;
    ngx_uint_t i, j;

    parsed_office_hours = malloc(7 * sizeof(ngx_uint_t *));

    hours = office_hours->elts;

    /*
     * On the configuration file, the leftmost element
     * always applies to all remaining days, all others
     * are read from right to left. So first we will apply
     * the initial override, and then iterate based on the
     * number of overrides
     */

    for (i = 0; i < WEEK_LENGTH + 1 - office_hours->nelts; ++i) {
        parsed_office_hours[i] = parse_office_hours_string(hours[0]);
    }

    for (i = 1; i < office_hours->nelts; ++i) {
        j = WEEK_LENGTH - office_hours->nelts + i;
        parsed_office_hours[j] = parse_office_hours_string(hours[i]);
    }

    return parsed_office_hours;
}

/*
 * Given a time string or the closed token, return a tuple
 * of numbers representing opening and closing hours
 */

static ngx_uint_t * parse_office_hours_string(ngx_str_t office_hours)
{

    int captures[(1 + 4) * 3];
    ngx_int_t n;
    ngx_uint_t * parsed_hours;

    parsed_hours = malloc(2 * sizeof(ngx_uint_t));

    if(ngx_strcmp(office_hours.data, CLOSED_TOKEN) == 0) {
        parsed_hours[0] = 0;
        parsed_hours[1] = 0;
        return parsed_hours;
    }

    n = ngx_regex_exec(rc.regex, &office_hours, captures, (1 + 4) * 3);

    if (n >= 0) {
        /* Opening Hours */

        parsed_hours[0] = 60 * 60 * parse_number(office_hours, captures[2], captures[3]);
        parsed_hours[0] = parsed_hours[0] + 60 * parse_number(office_hours, captures[4], captures[5]);

        parsed_hours[1] = 60 * 60 * parse_number(office_hours, captures[6], captures[7]);
        parsed_hours[1] = parsed_hours[1] + 60 * parse_number(office_hours, captures[8], captures[9]);

        return parsed_hours;
    }

    /* Non-matching strings count as open */

    parsed_hours[0] = 0;
    parsed_hours[1] = 86400;
    return parsed_hours;
}

/*
 * Given an office hours array, it returns whether or not
 * it is currently within office hours.
 */

static ngx_flag_t within_office_hours(ngx_uint_t ** office_hours)
{

    time_t now;
    ngx_uint_t day_of_week, seconds_of_day;
    ngx_uint_t * current_hours;

    ngx_time_update();
    now = ngx_time();
    day_of_week = get_day_of_week(now);
    seconds_of_day = get_seconds_of_day(now);
    current_hours = office_hours[day_of_week];

    return seconds_of_day >= current_hours[0] && seconds_of_day <= current_hours[1];
}

/*
 * Calculate the day of the week given a timestamp
 */

static ngx_uint_t get_day_of_week(time_t time)
{

    /* Epoch was thursday, so add 3 so we start on monday */
    return (time / 86400 + 3) % 7;
}

/*
 * Calculate the number of seconds elapsed today
 */

static ngx_uint_t get_seconds_of_day(time_t time)
{

    return time - (time / 86400) * 86400;
}

/*
 * Parses a string, returns 0 if match was not found
 */

static ngx_uint_t parse_number(ngx_str_t string, ngx_uint_t start, ngx_uint_t end)
{

    if (end - start == 0) {
        return 0;
    }

    return ngx_atoi(&string.data[start], end - start);
}

/*
 * Given the current office hours configuration it creates the custom
 * HTML
 */

static ngx_str_t create_output_html(ngx_http_office_hours_conf_t * conf)
{

    char * output_buffer;
    time_t now;
    ngx_str_t output_html;
    ngx_uint_t i, seconds_of_day;
    ngx_uint_t ** parsed_office_hours;


    output_buffer = malloc(1024 * sizeof(char));
    parsed_office_hours = parse_office_hours(conf->office_hours);
    now = ngx_time();
    seconds_of_day = get_seconds_of_day(now);

    sprintf(output_buffer, "%s", (char *) HEAD_HTML.data);

    for (i = 0; i < 7; ++i) {
        sprintf(output_buffer + strlen(output_buffer), "%s", format_hours(parsed_office_hours[i], (char *) DAY_NAMES[i]));
    }

    sprintf(output_buffer + strlen(output_buffer), "%s", (char *) MIDDLE_HTML.data);
    sprintf(output_buffer + strlen(output_buffer), "%s", format_seconds(seconds_of_day));
    sprintf(output_buffer + strlen(output_buffer), "%s", (char *) FOOT_HTML.data);

    output_html.data = (unsigned char *) output_buffer;
    output_html.len = strlen(output_buffer);

    return output_html;
}

/*
 * Given a tuple of seconds and a day name, outputs an HTML
 * string containing the formatted data as a list item
 */

static char * format_hours(ngx_uint_t * hours, char * day)
{

    char * output_html;

    output_html = malloc(64 * sizeof(char));
    if (hours[0] == hours[1]) {
        sprintf(output_html, "<li><strong>%s</strong>: CLOSED</li>",
                day
               );
        return output_html;
    }

    sprintf(output_html, "<li><strong>%s</strong>: %s - %s</li>",
            day,
            (char *) format_seconds(hours[0]),
            (char *) format_seconds(hours[1])
           );

    return output_html;
}

/*
 * Given seconds of the day, it returns a string showing
 * HH:MM in 24 hour format
 */

static char * format_seconds(ngx_uint_t seconds)
{

    char * output_html;
    unsigned int hours, minutes;

    output_html = malloc(6 * sizeof(char));

    hours = seconds / 60 / 60;
    minutes = (seconds / 60) % 60;

    sprintf(output_html, "%s:%s",
            left_pad(hours),
            left_pad(minutes)
           );

    return output_html;
}

/*
 * Returns a number as a string adding 0 if < 10
 */

static char * left_pad(unsigned int number)
{

    char * output_string;
    char * padding;

    padding = "";
    output_string = malloc(3 * sizeof(char));

    if (number < 10) {
        padding = "0";
    }

    sprintf(output_string, "%s%u", padding, number);

    return output_string;
}

/*
 * Postconfig Initialization Handler
 * Sets the request filter at the top of the chain
 */

static ngx_int_t ngx_http_office_hours_init(ngx_conf_t * cf)
{

    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_office_hours_header_filter;

    ngx_http_next_body_filter = ngx_http_top_body_filter;
    ngx_http_top_body_filter = ngx_http_office_hours_body_filter;

    rc.pattern = TIME_REGEX;
    rc.pool = cf->pool;
    if (ngx_regex_compile(&rc) != NGX_OK) {
        return NGX_ERROR;
    }

    return NGX_OK;
}
