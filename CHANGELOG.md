# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## 1.0.0 - 2018-12-28
### Added
- This CHANGELOG
- A README
- A CONTRIBUTING guide
- Formatting using astyle
- Git hook to format code before commit
- Makefile with tasks to format and install git hooks
- A gitignore file
- Parsing of directive time ranges in UTC only
- Dynamically generate HTML based on hours directive

[Unreleased]: https://gitlab.com/rbdr/ngx_office_hours/compare/master...develop
