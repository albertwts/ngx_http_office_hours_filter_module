setup_hooks:
	ln -s ../../scripts/git-hooks/pre-commit .git/hooks/pre-commit

format:
	astyle --lineend=linux --style=kr *.c
