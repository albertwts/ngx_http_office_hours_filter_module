# Contributing to ngx_http_office_hours_filter_module

This NGINX module has the simple goal of allowing you to specify office
hours for your server and not serving anything outside those hours.

Any contributions are welcome especially if it works towards that goal.

You can check the `README` for a list of **Further Improvements** if
you want inspiration. Also, since I'm not great at C, any improvements
to the code itself are also welcome!

## The objective of ngx_http_office_hours_filter_module

Give your web server the benefit of a regulated work schedule, and
notify the consumers of the appropriate office hours to visit the
website.

Providing better ways to specify the working schedule of the server
are in line with the objective.

## How to contribute

Above All: Be nice, always.

* Always make sure the code is properly formatted with `make format`
* Make the PRs according to [Git Flow][gitflow]: (features go to
  develop, hotfixes go to master)

[gitflow]: https://github.com/nvie/gitflow
