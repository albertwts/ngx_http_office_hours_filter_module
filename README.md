# Nginx Office Hours

A victory for the server's labor rights: An nginx module that allows you to serve your content only during office hours.

## Using the directive

The `office_hours` directive expects a list of time ranges separated by
spaces. The first range will be used as the default, and the rest will
be read from right to left, ending with *sunday*

### Example 1: Open 24 hours

Don't use this directive.

### Example 2: Open every day from 08:30 AM to 7:00 PM

```
office_hours 8:30-19;
```

Some things worth noting here: the format of time ranges uses 24 hour
style, so no pesky AMs or PMs. Likewise, minutes are optional. Finally,
note that since only one range was defined, this is applied to all days
of the week. Another noteworthy 

### Example 3: Open every day from 08:30 AM to 7:00 PM, but closed sundays

```
office_hours 8:30-19 closed;
```

Two important things here: first notice that the closed keyword
specifies that the server is closed on a specific day; second, notice
how the last element is used for sunday, while the first one still
applies to Monday-Saturday.

### Example 4: Open weekdays from 08:30 AM to 7:00 PM, Saturdays 10:00 AM to 4:00 PM, closed on sundays

```
office_hours 8:30-19 10-16 closed;
```

Now hopefully you can see the pattern: The first time range applies to
Monday-Friday, next one to Saturday and last one to Sunday.

### Example 5: Open weekdays from 08:30 AM to 7:00 PM, closed on thursdays, reduced hours on weekends.

```
office_hours 8:30-19 closed 8:30-19 10-16 10-16;
```

Since we had to specify from Thursday, we have to explicitly set the
values for Friday, Saturday and Sunday.

### Example 6: Closed every day.

Uninstall nginx.

## What you will need to get started

* [astyle][astyle]: Used to format the code
* An `nginx` [source distribution][nginx] to compile the code.
* A way to compile `nginx`. Check out [their guide][nginx-guide]!

## Building the module

You can build office hours as a dynamic module. From the `nginx` source
directory run:

```
./configure --add-dynamic-module=/path/to/ngx_http_office_hours_filter_module
make
```

For more information check the [nginx docs][nginx-module-docs]

## Installing git hooks

This repo contains a pre-commit git hook so indent will run before every
commit. Run `make setup_hooks` to install it.

## Further Improvements

* Add support for timezones (currently only GMT)
* Add support for public holidays
* Add support to respect the time zone of the visitor and not just the
  server
* Add support for double shifts in the same day

[astyle]: http://astyle.sourceforge.net
[nginx]: https://nginx.org/en/download.html
[nginx-guide]: https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/#sources
[nginx-module-docs]: https://www.nginx.com/resources/wiki/extending/
